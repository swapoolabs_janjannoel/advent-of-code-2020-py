# Advent of Code 2020 solutions

My solutions for [Advent of Code 2020](https://adventofcode.com/2020/) using Python 3.6.11 (still a python noob)

Some of these are already cleaned up and optimized and are not the actual code I used to submit my answers.
